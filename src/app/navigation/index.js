import React from 'react';
import Home from "../components/pages/Home"
import PoliticalDetail from "../components/pages/PoliticalDetail"
import { BrowserRouter,  Route, Redirect } from "react-router-dom";
import CssBaseline from '@material-ui/core/CssBaseline';
import Menubar from '../components/organisms/Menubar';
import Footer from '../components/organisms/Footer';

function Navigation() {
    return (
        <BrowserRouter basename="politis">
            <React.Fragment>
                <CssBaseline />
                <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
                <Menubar></Menubar>
                <main>
                    <Route exact path="/" render={() => (
                        <Redirect to="/home"/>
                    )}/>
                    <Route path="/home" exact component={Home} />                    
                    <Route path="/detail/:id" component={PoliticalDetail} />
                </main>
                <Footer></Footer>
            </React.Fragment>
        </BrowserRouter>

    )
}

export default Navigation;
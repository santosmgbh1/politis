import React from 'react';
import { connect } from 'react-redux'
import { filter, loadPoliticians } from '../../../store/actions/home'
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import styles from './styles';
import CardPolitical from '../../organisms/CardPolitical';
import Search from '../../molecules/Search';


class Home extends React.Component {

    componentDidMount() {
        const { fetchCandidatos, listCandidatos } = this.props        
        if (!listCandidatos)
            fetchCandidatos()
    }

    onSearch = (text) => {        
        const { fetchCandidatos } = this.props        
        fetchCandidatos(text)
    }

    render() {
        const { listCandidatos } = this.props
        const selectCard = (candidato) => {
            const { history } = this.props
            history.push({
                pathname: '/detail/' + candidato.id
            })
        }
        return (
            <div>                
                <div className={styles.heroContent}>
                    <Container style={{marginBottom: 10}}>
                        {/* <BeWelcome></BeWelcome> */}
                        <div className={styles.heroButtons}>
                            <Grid container spacing={2} justify="center">
                                <Grid item>
                                    <Search onSearch={this.onSearch}></Search>
                                </Grid>
                            </Grid>
                        </div>
                    </Container>
                </div>
                <Container className={styles.cardGrid} maxWidth="md">
                    {/* End hero unit */}
                    <Grid container spacing={4}>
                        {listCandidatos && listCandidatos.map(item => (
                            <Grid item key={item.id} xs={12} sm={2} md={2}>
                                <CardPolitical data={item} onClick={selectCard}></CardPolitical>
                            </Grid>
                        ))}
                    </Grid>
                </Container>
            </div>
        );
    }
}


const mapStateToProps = (state) => ({
    listCandidatos: state.home.listCandidatos
})

const mapDispatchToProps = {
    clickFilter: filter,
    fetchCandidatos: loadPoliticians
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Home)

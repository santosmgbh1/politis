import { makeStyles } from '@material-ui/core/styles';

const styles = makeStyles(theme => ({
    root: {
      flexGrow: 1,
    },
    container: {height: 100+'vh', backgroundColor: 'red'}
  }));

  export default styles;
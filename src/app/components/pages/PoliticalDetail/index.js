import React from 'react';
import { Container, Tabs, Tab } from "@material-ui/core";
import styles from './styles';
import { connect } from 'react-redux'
import { loadPolitical, loadDespesas, loadEventos, loadPropostas } from '../../../store/actions/home'
import Avatar from '@material-ui/core/Avatar';
import Despesas from '../../organisms/Despesas';
import Proposicoes from '../../organisms/Proposicoes';
import Eventos from '../../organisms/Eventos';
import Text from '../../atoms/Text';
import Grid from '@material-ui/core/Grid';


class PoliticalDetail extends React.Component {

    state = {
        tabIndex: 0
    }

    componentDidMount() {
        const { match, fetchPolitical, fetchDespesas, fetchEventos, fetchPropostas } = this.props;
        const { id } = match.params;
        fetchPolitical(id)
        fetchDespesas(id)
        fetchEventos(id)
        fetchPropostas(id)

    }

    handleTabChange = (event, newValue) => {
        this.setState({ tabIndex: newValue })
    }

    render() {
        const { political } = this.props;
        const { propostas, despesas, eventos } = this.props
        const { tabIndex } = this.state;
        return (
            <div className={styles.root}>
                <Container>
                    {political && 
                    <Grid container style={{marginTop: 10, marginBottom: 10}} >
                        <Grid xs={4} item >
                            <Text variant="subtitle1">Contato</Text>
                            <Text variant="subtitle2">{political.ultimoStatus.gabinete.email}</Text>
                            <Text variant="subtitle2">{political.ultimoStatus.gabinete.telefone}</Text>
                        </Grid>
                        <Grid xs={4} spacing={1} container direction="column" alignItems="center" justify="center" >
                            <Grid item>
                                <Avatar style={{ width: 150, height: 150 }} margin="dense" src={political.ultimoStatus.urlFoto}></Avatar>
                            </Grid>
                            <Grid item>
                                <Text variant="h5">{political.ultimoStatus.nomeEleitoral}</Text>
                            </Grid>
                        </Grid>
                        <Grid xs={4} item >
                            <Text variant="subtitle1">{political.ultimoStatus.situacao}</Text>
                            <Text variant="subtitle1">{political.ultimoStatus.siglaPartido + ' - ' + political.ultimoStatus.siglaUf}</Text>                            
                        </Grid>
                    </Grid>
                    }
                    <Grid container justify="center">
                        <Grid xs={12}>
                            <Tabs indicatorColor="primary"
                                textColor="primary"
                                variant="fullWidth"
                                value={tabIndex}
                                onChange={this.handleTabChange}>
                                <Tab label="Propostas" />
                                <Tab label="Despesas" />
                                <Tab label="Eventos" />
                            </Tabs>
                            {tabIndex === 0 && <Proposicoes list={propostas} ></Proposicoes>}
                            {tabIndex === 1 && <Despesas list={despesas} ></Despesas>}
                            {tabIndex === 2 && <Eventos list={eventos} ></Eventos>}
                        </Grid>
                    </Grid>
                </Container>
            </div>
        );
    }

}

PoliticalDetail.defaultProps = {
    political: { ultimoStatus: { gabinete: {} } }
}


const mapStateToProps = (state) => ({
    political: state.home.political,
    propostas: state.home.propostas,
    eventos: state.home.eventos,
    despesas: state.home.despesas
})

const mapDispatchToProps = {
    fetchPolitical: loadPolitical,
    fetchDespesas: loadDespesas,
    fetchEventos: loadEventos,
    fetchPropostas: loadPropostas
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(PoliticalDetail)
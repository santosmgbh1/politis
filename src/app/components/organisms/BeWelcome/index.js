import React from 'react';
import Typography from '@material-ui/core/Typography';

export default function BeWelcome() {
    return (
        <div>
            <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
                Politis
</Typography>
            <Typography variant="subtitle1" align="center" color="textSecondary" paragraph>
                Todos os dados são adquiridos na plataforma aberta da câmara legislativa, no endereço <b>https://dadosabertos.camara.leg.br</b>, portanto são de responsabilidade do mesmo
</Typography>
        </div>
    )
}
import React from 'react';
import { Paper, Grid } from '@material-ui/core';
import ListBox from '../../molecules/ListBox';
import ItemDespesa from '../../molecules/ItemDespesa';

function Despesas(props) {
    const {list} = props 
    return (        
        <Paper style={{minHeight: 300, maxHeight: 500, overflow: 'auto'}}>
        <Grid container direction="row" >            
            <Grid xs={12} item >                    
                <ListBox list={list} getItemView={(item) => (<ItemDespesa despesa={item} ></ItemDespesa>)}></ListBox>                
            </Grid>
        </Grid> 
        </Paper>                                   
    )
}

export default Despesas;
import React from 'react';
import Typography from '@material-ui/core/Typography';
import classes from './styles';

export default function Footer() {
    const styles = classes()
    return (
        <footer className={styles.footer}>
            <Typography variant="h6" align="center" gutterBottom>
                Politis
        </Typography>
            <Typography variant="subtitle1" align="center" color="textSecondary" component="p">
                Desenvolvimento com propósito acadêmico e informativo por Almir Santos
            </Typography>
            <Typography variant="subtitle1" align="center" color="textSecondary" component="p">
                2019
            </Typography>
        </footer>
    )
}

import { makeStyles } from '@material-ui/core/styles';

const styles = makeStyles(theme => ({
    icon: {
      marginRight: theme.spacing(2),
    }
  }));

  export default styles;
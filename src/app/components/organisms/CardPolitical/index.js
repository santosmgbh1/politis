import React from 'react';
import { Card, CardActionArea, CardContent, CardMedia, Typography } from '@material-ui/core';
import classes from './styles';

export default function CardPolitical(props) {
    const styles = classes()
    const { data } = props
    return (<Card className={styles.card}>
        <CardActionArea onClick={() => props.onClick(data)}>
            <CardMedia
                className={styles.cardMedia}
                image={data.urlFoto}
                title="Image title"
            />
            <CardContent className={styles.cardContent}>
                <Typography gutterBottom variant="subtitle1" component="h2">
                    {data.nome}
                </Typography>
                <Typography>
                    {data.siglaUf} | {data.siglaPartido}
                </Typography>
            </CardContent>
        </CardActionArea>
    </Card >)
}
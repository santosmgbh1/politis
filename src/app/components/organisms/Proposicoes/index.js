import React from 'react';
import { Paper, Grid } from '@material-ui/core';
import ListBox from '../../molecules/ListBox';
import ItemProposta from '../../molecules/ItemProposta';
function Proposicoes(props) {
    const {list} = props 
    return (        
        <Paper style={{minHeight: 300, maxHeight: 500, overflow: 'auto'}}>
        <Grid container direction="row" >            
            <Grid xs={12} item >                    
                <ListBox list={list} getItemView={(item) => (<ItemProposta proposta={item} ></ItemProposta>)}></ListBox>                
            </Grid>
        </Grid> 
        </Paper>                                   
    )
}

export default Proposicoes;
import React from 'react';
import { ListItem, ListItemSecondaryAction, ListItemText } from '@material-ui/core';
import Text from '../../atoms/Text';

import moment from 'moment';

const formatDate = (date) => {
    var m = moment(date);    
    return m.format('DD/MM/YYYY');
}

export default function ItemEvento({ evento }) {
    return (
        <ListItem button>
            <ListItemText primary={formatDate(evento.dataHoraInicio)} secondary={                
                <Text component="span" variant="body2" wrap>{evento.descricao}</Text>                
            } />
            <ListItemSecondaryAction>
            {evento.localCamara && (
                <Text component="span" variant="body2" wrap>{evento.localCamara.nome}</Text>
            )}                
            </ListItemSecondaryAction>
        </ListItem>
    )
}
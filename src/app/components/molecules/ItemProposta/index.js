import React from 'react';
import { ListItem, ListItemSecondaryAction, ListItemText } from '@material-ui/core';
import Text from '../../atoms/Text';

export default function ItemProposta({ proposta }) {
    return (
        <ListItem button>
            <ListItemText primary={proposta.siglaTipo} secondary={                
                <Text component="span" variant="body2" wrap>{proposta.ementa}</Text>                
            } />
            <ListItemSecondaryAction>
                <Text component="span" variant="body2" wrap>{proposta.ano}</Text>
            </ListItemSecondaryAction>
        </ListItem>
    )
}
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { List, Divider } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
    root: {      
      backgroundColor: theme.palette.background.paper
    },
  }));

function ListBox(props) {
    const classes = useStyles();
    const { list, getItemView } = props;
    return (
        <List className={classes.root}>
            {list.map(item => (
                <div>
                    {getItemView(item)}
                    <Divider light />
                </div>
            ))}

        </List>
    )
}

ListBox.defaultProps = {
    list: []
}

export default ListBox;
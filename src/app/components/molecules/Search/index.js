import React from 'react';
import { TextField, Box } from "@material-ui/core";
import InputAdornment from '@material-ui/core/InputAdornment';
import SearchIcon from '@material-ui/icons/Search';

export default class Search extends React.Component {

    state = {
        text: ''
    }
    handleChange = (event) => {
        this.setState({text: event.target.value});
        if (event.target.value != null) {
            const { onSearch } = this.props;
            if (onSearch)
                onSearch(event.target.value)
        }
    }
    render() {        
        const { text } = this.state
        return (
            <Box display="flex">
                <TextField placeholder="Busque pelo nome..." value={text} 
                onChange={this.handleChange}
                InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <SearchIcon />
                      </InputAdornment>
                    ),
                  }}></TextField>                
            </Box>
        );
    }
}


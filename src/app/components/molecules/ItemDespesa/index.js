import React from 'react';
import { ListItem, ListItemSecondaryAction, ListItemText } from '@material-ui/core';
import Text from '../../atoms/Text';
import moment from 'moment';

const formatValor = (valor) => {
    return 'R$ ' + valor.toFixed(2);
}

const formatDate = (date) => {
    var m = moment(date);    
    return m.format('DD/MM/YYYY');
}
export default function ItemDespesa({ despesa }) {
    return (
        <ListItem button>
            <ListItemText primary={despesa.nomeFornecedor} secondary={          
                <React.Fragment>
                    {formatDate(despesa.dataDocumento) + ' - '}
                    <Text component="span" variant="body2" wrap>{despesa.tipoDespesa}</Text>                
                </React.Fragment>                      
            } />
            <ListItemSecondaryAction>
                <Text component="span" variant="body2" wrap>{formatValor(despesa.valorDocumento)}</Text>
            </ListItemSecondaryAction>
        </ListItem>
    )
}
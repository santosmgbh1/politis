import { getDespesas, getPropostas, getEventos, getPolitical, getPoliticians } from '../../services/PoliticiansService'

export const filter = text => dispatch => {
  const a = { opa: text }
  console.log('etaa bosta')
  dispatch({ type: 'GET_POLITICS', a })
}

export const loadPoliticians = (text) => dispatch => {
  getPoliticians(text).then(response => {
    let {dados} = response.data
    let payload = dados
    dispatch({ type: 'GET_POLITICIANS', payload })
  }).catch(error => {
    console.log(error)
    throw error
  })
}

export const loadPolitical = id => dispatch => {
  getPolitical(id).then(response => {    
    const {dados} = response.data
    let payload = dados
    dispatch({ type: 'GET_POLITICAL', payload })
  }).catch(error => {
    console.log(error)
  })  
}

export const loadPropostas = id => dispatch => {
  getPropostas(id).then(response => {    
    const {dados} = response.data    
    let payload = dados;
    dispatch({ type: 'GET_PROPOSTAS', payload })
  }).catch(error => {
    console.log(error)
  })  
}

export const loadDespesas = id => dispatch => {
  getDespesas(id).then(response => {    
    const {dados} = response.data
    let payload = dados;
    dispatch({ type: 'GET_DESPESAS', payload })
  }).catch(error => {
    console.log(error)
  })  
}

export const loadEventos = id => dispatch => {
  getEventos(id).then(response => {    
    const {dados} = response.data
    let payload = dados;
    dispatch({ type: 'GET_EVENTOS', payload })
  }).catch(error => {
    console.log(error)
  })  
}
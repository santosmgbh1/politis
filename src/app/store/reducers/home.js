const home = (state = [], action) => {
    switch (action.type) {
      case 'GET_POLITICS':
        return [
          ...state,
          action.payload
        ]
      case 'GET_POLITICIANS':
        return {
          ...state,
          listCandidatos: action.payload
        }
      case 'GET_POLITICAL':
      return {
        ...state,
        political: action.payload
      }        
      case 'GET_DESPESAS':
      return {
        ...state,
        despesas: action.payload
      }        
      case 'GET_EVENTOS':
      return {
        ...state,
        eventos: action.payload
      }        
      case 'GET_PROPOSTAS':
      return {
        ...state,
        propostas: action.payload
      }        
      default:
        return state
    }
  }
  
  export default home
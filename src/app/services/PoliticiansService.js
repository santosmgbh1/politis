import axios from 'axios';

export const getPoliticians = function (nome) {
    if(nome) {
        return axios({ method: 'get', url: 'https://dadosabertos.camara.leg.br/api/v2/deputados?nome='+nome+'&ordem=ASC&ordenarPor=nome', responseType: 'application/json' })    
    }    
    return axios({ method: 'get', url: 'https://dadosabertos.camara.leg.br/api/v2/deputados?ordem=ASC&itens=50&ordenarPor=nome', responseType: 'application/json' })    
}

export const getPolitical = function (id) {
    return axios({method: 'get', url:'https://dadosabertos.camara.leg.br/api/v2/deputados/' + id, responseType: 'application/json'})
}

export const getPropostas = function (id) {
    return axios({method: 'get', url:'https://dadosabertos.camara.leg.br/api/v2/proposicoes?idDeputadoAutor='+id+'&ordem=ASC&ordenarPor=id', responseType: 'application/json'})
}

export const getDespesas = function (id) {
    return axios({method: 'get', url:'https://dadosabertos.camara.leg.br/api/v2/deputados/'+id+'/despesas?ordem=DESC&ordenarPor=dataDocumento', responseType: 'application/json'})
}
export const getEventos = function (id) {
    return axios({method: 'get', url:'https://dadosabertos.camara.leg.br/api/v2/deputados/'+id+'/eventos?ordem=ASC&ordenarPor=dataHoraInicio', responseType: 'application/json'})
}
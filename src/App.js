import React from 'react';
import './App.css';
import { Provider } from 'react-redux'
import store from './app/store'
import Navigation from './app/navigation';

function App() {
  document.title = 'Politis'
  return (
    <Provider store={store}>
      <div className="App">
        <Navigation></Navigation>        
      </div>
    </Provider >
  );
}

export default App;
